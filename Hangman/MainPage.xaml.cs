﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using HangmanNeon.Resources;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Phone.Tasks;
using System.Xml.Linq;
using System.IO;
using Microsoft.Advertising.Mobile;



namespace HangmanNeon
{
    public partial class MainPage : PhoneApplicationPage
    {

        String answer;
        int answerLength;
        int charsGuessed;
        int incorrectCount;
        int gamesPlayed = 0;
        int gamesWon = 0;
        int winsInRow = 0;
        int maxWinsRow = 0;
        int gamesLost = 0;
        int gamesWordLength = 0;
        //int gamesEarliestStage = 0;
        int newkidStage = 0;
        int appChallengeStage = 0;
        int proStage = 0;

        string[] ANIMALS = {"AARDVARK", "ALLIGATOR", "ALPACA", "ANTEATER", "ANTELOPE", "APE", "ARMADILLO", "BABOON", "BADGER", 
                            "BASILISK", "BAT", "BEAR", "BEAVER", "BISON", "BOAR", "BUFFALO", "BULL", "CAMEL", "CANARY", "CAT", 
                            "CHAMELEON", "CHEETAH", "CHIMPANZEE", "CHINCHILLA", "CHIPMUNK", "COUGAR", "COW", "COYOTE", "CROCODILE", 
                            "CROW", "DEER", "DINGO", "DOG", "DONKEY", "DORMOUSE", "ELAND", "ELEPHANT", "ELK", "FERRET", "FINCH", 
                            "FOX", "FROG", "GAZELLE", "GIRAFFE", "GNU", "GOAT", "GOPHER", "GORILLA", "HAMSTER", "HARE", "HEDGEHOG", 
                            "HOG", "HORSE", "HYENA", "IGUANA", "IMPALA", "JACKAL", "JAGUAR", "KANGAROO", "KOALA", "LEMUR", "LEOPARD", 
                            "LION", "LIZARD", "LLAMA", "LYNX", "MANDRILL", "MARMOSET", "MINK", "MOLE", "MONGOOSE", "MONKEY", "MOOSE", 
                            "MOUSE", "MULE", "MUSKRAT", "MUSTANG", "NEWT", "OCELOT", "ORANGUTAN", "OTTER", "OX", "PANDA", "PANTHER", 
                            "PARAKEET", "PARROT", "PIG", "PLATYPUS", "PONY", "PORCUPINE", "PORPOISE", "PUMA", "RABBIT", "RACCOON", 
                            "RAM", "RAT", "REINDEER", "RHINOCEROS", "ROEBUCK", "SALAMANDER", "SEAL", "SHEEP", "SHREW", "SKUNK", "SLOTH", 
                            "SNAKE", "SPRINGBOK", "SQUIRREL", "TAPIR", "TIGER", "TOAD", "TURTLE", "WALRUS", "WARTHOG", "WEASEL", "WHALE", 
                            "WILDCAT", "WOLF", "WOMBAT", "YAK", "ZEBRA"};

        string[] MINERALS = {"ACANTHITE", "ACTINOLITE", "ADAMITE", "AEGIRINE", "AFGHANITE", "AGATE", "ALBITE", "ALMANDINE", "AMETHYST", 
                             "ANALCIME", "ANATASE", "ANDALUSITE", "ANDESINE", "ANDRADITE", "ANGLESITE", "ANHYDRITE", "ANORTHITE", "ANTIMONY", 
                             "APATITE", "AQUAMARINE", "ARAGONITE", "ARSENIC", "AUGITE", "AXINITE", "AZURITE", "BARITE", "BAUXITE", "BENITOITE", 
                             "BERYL", "BIOTITE", "BISMUTH", "BOEHMITE", "BORAX", "BORNITE", "BROOKITE", "BUERGERITE", "BUSTAMITE", "BYTOWNITE", 
                             "CALCITE", "CELESTINE", "CELSIAN", "CERUSSITE", "CERVANTITE", "CHABAZITE", "CHALCEDONY", "CHALCOCITE", "CHAMOSITE", 
                             "CHLORITE", "CHROMITE", "CINNABAR", "CITRINE", "COESITE", "COLEMANITE", "COOKEITE", "COPPER", "CORDIERITE", "CORUNDUM", 
                             "CROCOITE", "CUPRITE", "DANBURITE", "DIAMOND", "DIASPORE", "DIOPSIDE", "DIOPTASE", "DOLOMITE", "DRAVITE", "EDENITE", 
                             "ELBAITE", "EMERALD", "ENSTATITE", "EPIDOTE", "EPSOMITE", "ERYTHRITE", "EUCLASE", "FERBERITE", "FLUORITE", "GALENA", 
                             "GARNET", "GASPEITE", "GIBBSITE", "GLAUBERITE", "GMELINITE", "GOETHITE", "GOLD", "GRAPHITE", "GROSSULAR", "GYPSUM", 
                             "HAFNON", "HALITE", "HEMATITE", "HEULANDITE", "HORNBLENDE", "HOWLITE", "HUEBNERITE", "ILMENITE", "JADEITE", "JAROSITE", 
                             "JASPER", "KAOLINITE", "KERNITE", "KYANITE", "LAUMONTITE", "LAZULITE", "LAZURITE", "LEAD", "LEPIDOLITE", "LEUCITE", 
                             "LIMONITE", "LINARITE", "MAGNESITE", "MAGNETITE", "MALACHITE", "MARCASITE", "MARIALITE", "MEIONITE", "MERCURY", 
                             "MESOLITE", "MICROCLINE", "MIMETITE", "MUSCOVITE", "NATROLITE", "NEPHELINE", "NEPTUNITE", "OLIGOCLASE", "OLIVINE", 
                             "OMPHACITE", "OPAL", "ORPIMENT", "ORTHOCLASE", "OTAVITE", "PARADAMITE", "PARGASITE", "PECTOLITE", 
                             "PHENAKITE", "PHLOGOPITE", "PHOSGENITE", "PLATINUM", "POWELLITE", "PREHNITE", "PYRITE", "PYROPE", "QUARTZ", "RASPITE", 
                             "REALGAR", "RHENIITE", "RHODONITE", "RIEBECKITE", "RUTILE", "SAFFLORITE", "SANIDINE", "SCAPOLITE", "SCHEELITE", "SCHORL", 
                             "SCOLECITE", "SCORZALITE", "SERANDITE", "SERPENTINE", "SIDERITE", "SILVER", "SODALITE", "SPHALERITE", "SPINEL", "SPODUMENE", 
                             "STAUROLITE", "STELLERITE", "STIBARSEN", "STIBNITE", "STILBITE", "STISHOVITE", "STOLZITE", "STRENGITE", "SULFUR", "TELLURIUM", 
                             "TEPHROITE", "THENARDITE", "THOMSONITE", "THORITE", "TOPAZ", "TOURMALINE", "TREMOLITE", "TRIDYMITE", "TURQUOISE", "ULEXITE", 
                             "URANINITE", "UVAROVITE", "UVITE", "VANADINITE", "VARISCITE", "VIVIANITE", "WAVELLITE", "WILLEMITE", "WITHERITE", "WOLFRAMITE", 
                             "WULFENITE", "WURTZITE", "ZINCITE", "ZIRCON", "ZOISITE"};

        string[] CITIES = {"ABERDEEN", "BASILDON", "BELFAST", "BIRMINGHAM", "BLACKBURN", "BLACKPOOL", "BOLTON", "BRADFORD", "BRIGHTON", 
                           "BRISTOL", "CAMBRIDGE", "CARDIFF", "CHELMSFORD", "CHELTENHAM", "COLCHESTER", "COVENTRY", "CRAWLEY", "DERBY", 
                           "DUDLEY", "DUNDEE", "EDINBURGH", "EXETER", "GATESHEAD", "GLASGOW", "GLOUCESTER", "IPSWICH", "LEEDS", "LEICESTER", 
                           "LIVERPOOL", "LONDON", "LUTON", "MAIDSTONE", "MANCHESTER", "NEWCASTLE", "NEWPORT", "NORWICH", "NOTTINGHAM", 
                           "OLDHAM", "OXFORD", "PLYMOUTH", "POOLE", "PORTSMOUTH", "PRESTON", "READING", "ROTHERHAM", "SHEFFIELD", "SHREWSBURY", 
                           "SLOUGH", "SOUTHEND", "STOCKPORT", "SUNDERLAND", "SWANSEA", "SWINDON", "TELFORD", "WALSALL", "WARRINGTON", "YORK"};

        string[] KS1 = { "ABOUT", "AFTER", "AGAIN", "AN", "ANOTHER", "AS", "BACK", "BALL", "BE", "BECAUSE", "BED", "BEEN", 
                         "BOY", "BROTHER", "BUT", "BY", "CALL", "CALLED", "CAME", "CAN'T", "COULD", "DID", "DO", "DON'T", 
                         "DIG", "DOOR", "DOWN", "FIRST", "FROM", "GIRL", "GOOD", "GOT", "HAD", "HALF", "HAS", "HAVE", "HELP", 
                         "HER", "HERE", "HIM", "HIS", "HOME", "HOUSE", "HOW", "IF", "JUMP", "JUST", "LAST", "LAUGH", "LITTLE", 
                         "LIVE", "LIVED", "LOVE", "MADE", "MAKE", "MAN", "MANY", "MAY", "MORE", "MUCH", "MUST", "NAME", "NEW", 
                         "NEXT", "NIGHT", "NOT", "NOW", "OFF", "OLD", "ONCE", "ONE", "OR", "OUR", "OUT", "OVER", "PEOPLE", "PUSH", 
                         "PULL", "PUT", "RAN", "SAW", "SCHOOL", "SEEN", "SHOULD", "SISTER", "SO", "SOME", "TAKE", "THAN", "THAT", 
                         "THEIR", "THEM", "THEN", "THERE", "THESE", "THREE", "TIME", "TOO", "TOOK", "TREE", "TWO", "US", "VERY", 
                         "WANT", "WATER", "WAY", "WERE", "WHAT", "WHEN", "WHERE", "WHO", "WILL", "WITH", "WOULD", "YOUR", "MONDAY", 
                         "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY", "JANUARY", "FEBRUARY", "MARCH", "APRIL", 
                         "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER", "ONE", "TWO", "THREE", "FOUR", 
                         "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", 
                         "SEVENTEEN", "EIGHTEEN", "NINETEEN", "TWENTY", "BLACK", "WHITE", "RED", "YELLOW", "BLUE", "GREEN", "ORANGE", "PINK", 
                         "GREY", "PURPLE", "BROWN"};

        string[] NUMBERS = { "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", 
                             "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN", "TWENTY", "THIRTY", "FORTY", "FIFTY", 
                             "SIXTY", "SEVENTY", "EIGHTY", "NINETY", "HUNDRED", "THOUSAND" };

        string[] TREES = { "ALDER", "ASH", "ASPEN", "BEECH", "BIRCH", "ELDER", "ELM", "FIR", "HAWTHORN", "HAZEL", "HEMLOCK", "HOLLY", "HORNBEAM", 
                           "LARCH", "MAPLE", "OAK", "PINE", "POPLAR", "REDWOOD", "ROWAN", "SPRUCE", "SYCAMORE", "WILLOW", "YEW"};


        string[] VEGETABLES = { "APPLE", "ARTICHOKE", "ASPARAGUS", "AVOCADO", "BASIL", "BEANS", "BEETROOT", "BLACKBERRY", "BLUEBERRY", "BROCCOLI", 
                                "CABBAGE", "CAPERS", "CARROT", "CELERIAC", "CELERY", "CHARD", "CHICORY", "CHIVES", "CORN", "CUCUMBER", "DILL", 
                                "FENNEL", "FIG", "GARLIC", "GHERKIN", "GINGER", "GINSENG", "GOURDS", "GRAPE", "GUAVA", "JACKFRUIT", "KALE", "LEEK", 
                                "LENTILS", "LETTUCE", "LYCHEE", "MACADAMIA", "MANGO", "MELON", "MUSHROOM", "MUSTARD", "OKRA", "ONION", "PAPAYA", 
                                "PAPRIKA", "PARSLEY", "PARSNIP", "PEAR", "PEAS", "PECAN", "PEPPER", "PINEAPPLE", "POTATO", "PUMPKIN", "RADISH", 
                                "RASPBERRY", "RHUBARB", "SAFFRON", "SHALLOT", "SPINACH", "SQUASH", "SWEETCORN", "TOMATO", "TURNIP", "WATERCRESS", 
                                "WATERMELON", "YAMS", "SWEDE", "BANANA" };
        

        string[] wordlist;

        Random rand = new Random();
		

        // Constructor
        public MainPage()
        {
            InitializeComponent();

           //capture Microsoft Ad bugs
            AdUnit.ErrorOccurred += AdUnit_ErrorOccurred;
            AdUnit2.ErrorOccurred += AdUnit2_ErrorOccurred;
            AdUnit3.ErrorOccurred += AdUnit3_ErrorOccurred;
            AdUnit3.ErrorOccurred += AdUnit3_ErrorOccurred;
            InneractiveXamlAd.AdFailed += InneractiveXamlAd_AdFailed;
            //Set App version from Assembly
            aboutAppVer.Text = "Version " + Assembly.GetExecutingAssembly().GetName().Version;

            // Set the data context of the listbox control to the sample data
//            DataContext = App.ViewModel;

            // Test data.
            List<string> listData = new List<string>();
            listData.Add("Animals");
            listData.Add("Cities");
            listData.Add("Fruit & Vegetables");
            listData.Add("Minerals");
            listData.Add("Numbers");
            listData.Add("Trees");
            //            listData.Add("KS1 Words");


            // Bind to control.
            MyLongListSelector.ItemsSource = listData;

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();

            //Choose Category
            randomCategory();

            //Get game counts - you lemon!!

            getGameCounts();


            //Get word to guess and reset display
            resetApp();
			

        }

        void InneractiveXamlAd_AdFailed(object sender)
        {
            //throw new NotImplementedException();
        }

        // Load data for the ViewModel Items
        private void getGameCounts()
        {
            string[] scores;
            string[] scoreCategory;

            //            var xml = @"<?xml version=""1.0"" encoding=""utf-8"" ?>
            //            <hangman>
            //            <file>Hangman Scores</file>
            //            <item>
            //            <title>Games Played</title>
            //            <score>1</score>
            //            </item>
            //            <item>
            //            <title>Games Won</title>
            //            <score>2</score>
            //            </item>
            //            <item>
            //            <title>Games Lost</title>
            //            <score>3</score>
            //            </item>
            //            <item>
            //            <title>Longest Word</title>
            //            <score>4</score>
            //            </item>
            //            </hangman>";
            //var doc = XDocument.Parse(xml);

            //Initialise Counts
            GamesPlayed.Text = "0";
            GamesWon.Text = "0";
            GamesLost.Text = "0";
            LongestWord.Text = "";


            // Obtain a virtual store for the application.
            System.IO.IsolatedStorage.IsolatedStorageFile local =
                System.IO.IsolatedStorage.IsolatedStorageFile.GetUserStoreForApplication();

            // Specify the file path and options.
            XDocument doc;

            //Try to get scores from local XML File
            try
            {
                var isoFileStream = new System.IO.IsolatedStorage.IsolatedStorageFileStream
                        ("DataFolder\\Scores.xml", System.IO.FileMode.Open, local);
                doc = XDocument.Load(isoFileStream);

            }
            catch (System.IO.IsolatedStorage.IsolatedStorageException)
            {
                //Get default scores from XML File if local file not found
                doc = XDocument.Load("Assets/Scores.xml");
            }
            catch (System.Xml.XmlException)
            {
                MessageBox.Show("Corrupt Scores File. Resetting Game Scores", "App Initialization", MessageBoxButton.OK);
                //Get default scores from XML File if XML file corrupt
                doc = XDocument.Load("Assets/Data/Scores.xml");
            }
   

            scoreCategory = doc.Descendants("title").Select(o => o.Value).ToArray();
            scores = doc.Descendants("score").Select(o => o.Value).ToArray();

            for (int i = 0; i < scoreCategory.Count(); i++)
            {
                if (scoreCategory[i] == "Games Played")
                {
                    GamesPlayed.Text = scores[i];
                    gamesPlayed = Convert.ToInt32(scores[i]);
                }
                if (scoreCategory[i] == "Games Won")
                {
                    GamesWon.Text = scores[i];
                    gamesWon = Convert.ToInt32(scores[i]);
                    badgeChange("Count", gamesWon);
                }
                if (scoreCategory[i] == "Wins In Row")
                {
                    winsInRow = Convert.ToInt32(scores[i]);
                }
                if (scoreCategory[i] == "Games Lost")
                {
                    GamesLost.Text = scores[i];
                    gamesLost = Convert.ToInt32(scores[i]);
                    badgeChange("Loser", gamesLost);
                }
                
                if (scoreCategory[i] == "Longest Word")
                {
                    LongestWord.Text = scores[i];
                    if (LongestWord.Text.Length > 0)
                    {
                        gamesWordLength = Convert.ToInt32(scores[i]);
                    }
                }

                if (scoreCategory[i] == "NewKid Badge")
                {
                    if (scores[i].Length > 0)
                    {
                        newkidStage = Convert.ToInt32(scores[i]);
                        badgeChange("NewKid", newkidStage);
                    }
                }

                if (scoreCategory[i] == "Row Badge")
                {
                    if (scores[i].Length > 0)
                    {
                        maxWinsRow = Convert.ToInt32(scores[i]);
                        badgeChange("RowWin", maxWinsRow);
                    }
                }

                if (scoreCategory[i] == "Pro Badge")
                {
                    if (scores[i].Length > 0)
                    {
                        proStage = Convert.ToInt32(scores[i]);
                        badgeChange("Pro", proStage);
                    }
                }

                if (scoreCategory[i] == "AppChallenge Badge")
                {
                    if (scores[i].Length > 0)
                    {
                        appChallengeStage = Convert.ToInt32(scores[i]);
                        badgeChange("AppChallenge", appChallengeStage);
                    }
                }
            }

        }

        // Load data for the ViewModel Items
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (!App.ViewModel.IsDataLoaded)
            {
                App.ViewModel.LoadData();
            }
        }

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}

            private void A_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('A');

            A_Button.Visibility = System.Windows.Visibility.Collapsed;

        }

        private void B_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('B');

            B_Button.Visibility = System.Windows.Visibility.Collapsed;

        }

        private void C_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('C');

            C_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void D_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('D');

            D_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void E_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('E');

            E_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void F_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('F');

            F_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void G_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('G');

            G_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void H_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('H');

            H_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void I_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('I');

            I_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void J_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('J');

            J_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void K_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('K');

            K_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void L_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('L');

            L_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void M_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('M');

            M_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void N_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('N');

            N_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void O_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('O');

            O_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void P_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('P');

            P_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void Q_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('Q');

            Q_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void R_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('R');

            R_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void S_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('S');

            S_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void T_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('T');

            T_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void U_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('U');

            U_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void V_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('V');

            V_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void W_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('W');

            W_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void X_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('X');

            X_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void Y_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('Y');

            Y_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void Z_Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // TODO: Add event handler implementation here.


            LetterPosition('Z');

            Z_Button.Visibility = System.Windows.Visibility.Collapsed;


        }

        private void resetApp()
        {
        	// Used to reset app when loaded or new game started.
			AnswerChar1.Text = "?";
            AnswerChar1.Foreground = new SolidColorBrush(Colors.Yellow);
			AnswerChar2.Text = "?";
            AnswerChar2.Foreground = new SolidColorBrush(Colors.Yellow);
			AnswerChar3.Text = "?";
            AnswerChar3.Foreground = new SolidColorBrush(Colors.Yellow);
			AnswerChar4.Text = "?";
            AnswerChar4.Foreground = new SolidColorBrush(Colors.Yellow);
			AnswerChar5.Text = "?";
            AnswerChar5.Foreground = new SolidColorBrush(Colors.Yellow);
			AnswerChar6.Text = "?";
            AnswerChar6.Foreground = new SolidColorBrush(Colors.Yellow);
			AnswerChar7.Text = "?";
            AnswerChar7.Foreground = new SolidColorBrush(Colors.Yellow);
			AnswerChar8.Text = "?";
            AnswerChar8.Foreground = new SolidColorBrush(Colors.Yellow);
			AnswerChar9.Text = "?";
            AnswerChar9.Foreground = new SolidColorBrush(Colors.Yellow);
			AnswerChar10.Text = "?";
            AnswerChar10.Foreground = new SolidColorBrush(Colors.Yellow);

            // reset hangman picture and end game message
            incorrectCount = 0;
            ChangeHangmanPicture(incorrectCount);
            EndGame.Visibility = System.Windows.Visibility.Collapsed;
            Win_Picture.Visibility = System.Windows.Visibility.Collapsed;
            EndGame.Text = "";
            charsGuessed = 0;
           
            //ensure letters can be selected
            LetterVisible(true);


			// Determine word to be guessed
			answer = NewWord();
			
			//Determine length of answer
			answerLength = answer.Length;
			
			//Setup Answer Fields, dependant on length of word
			
			switch (answerLength)
			{
				case 1:
				{
					this.AnswerChar1.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar2.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar3.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar4.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar5.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar6.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar7.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar8.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar9.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar10.Visibility = System.Windows.Visibility.Collapsed;
					
					break;
				}
					
				case 2:
				{
					this.AnswerChar1.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar2.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar3.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar4.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar5.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar6.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar7.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar8.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar9.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar10.Visibility = System.Windows.Visibility.Collapsed;
					
					break;
				}
					
				case 3:
				{
					this.AnswerChar1.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar2.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar3.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar4.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar5.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar6.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar7.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar8.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar9.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar10.Visibility = System.Windows.Visibility.Collapsed;
					
					break;
				}
					
				case 4:
				{
					this.AnswerChar1.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar2.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar3.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar4.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar5.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar6.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar7.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar8.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar9.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar10.Visibility = System.Windows.Visibility.Collapsed;
					
					break;
				}
					
				case 5:
				{
					this.AnswerChar1.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar2.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar3.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar4.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar5.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar6.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar7.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar8.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar9.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar10.Visibility = System.Windows.Visibility.Collapsed;
					
					break;
				}
					
				case 6:
				{
					this.AnswerChar1.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar2.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar3.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar4.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar5.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar6.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar7.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar8.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar9.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar10.Visibility = System.Windows.Visibility.Collapsed;
					
					break;
				}
					
				case 7:
				{
					this.AnswerChar1.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar2.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar3.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar4.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar5.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar6.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar7.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar8.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar9.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar10.Visibility = System.Windows.Visibility.Collapsed;
					
					break;
				}
					
				case 8:
				{
					this.AnswerChar1.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar2.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar3.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar4.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar5.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar6.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar7.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar8.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar9.Visibility = System.Windows.Visibility.Collapsed;
					this.AnswerChar10.Visibility = System.Windows.Visibility.Collapsed;
					
					break;
				}
					
				case 9:
				{
					this.AnswerChar1.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar2.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar3.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar4.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar5.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar6.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar7.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar8.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar9.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar10.Visibility = System.Windows.Visibility.Collapsed;
					
					break;
				}
					
				case 10:
				{
					this.AnswerChar1.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar2.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar3.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar4.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar5.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar6.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar7.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar8.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar9.Visibility = System.Windows.Visibility.Visible;
					this.AnswerChar10.Visibility = System.Windows.Visibility.Visible;
					
					break;
				}
					
			}
			
        }

        

        private void LetterEnabled(bool enabled)
        {
            A_Button.IsEnabled = enabled;
            B_Button.IsEnabled = enabled;
            C_Button.IsEnabled = enabled;
            D_Button.IsEnabled = enabled;
            E_Button.IsEnabled = enabled;
            F_Button.IsEnabled = enabled;
            G_Button.IsEnabled = enabled;
            H_Button.IsEnabled = enabled;
            I_Button.IsEnabled = enabled;
            J_Button.IsEnabled = enabled;
            K_Button.IsEnabled = enabled;
            L_Button.IsEnabled = enabled;
            M_Button.IsEnabled = enabled;
            N_Button.IsEnabled = enabled;
            O_Button.IsEnabled = enabled;
            P_Button.IsEnabled = enabled;
            Q_Button.IsEnabled = enabled;
            R_Button.IsEnabled = enabled;
            S_Button.IsEnabled = enabled;
            T_Button.IsEnabled = enabled;
            U_Button.IsEnabled = enabled;
            V_Button.IsEnabled = enabled;
            W_Button.IsEnabled = enabled;
            X_Button.IsEnabled = enabled;
            Y_Button.IsEnabled = enabled;
            Z_Button.IsEnabled = enabled;
        }

        private void LetterVisible(bool visible)
        {
            Visibility isVisible;
            if (visible)
            {
                isVisible = System.Windows.Visibility.Visible;
                // hide Ad each time letters refreshed.
                if (AdUnit2 != null)
                {
                    AdUnit2.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
            else
            {
                isVisible = System.Windows.Visibility.Collapsed;
                // Present Ad at end of each game.
                AdUnit2.Visibility = System.Windows.Visibility.Visible;
                AdUnit2.Refresh();

            }
                A_Button.Visibility = isVisible;
                B_Button.Visibility = isVisible;
                C_Button.Visibility = isVisible;
                D_Button.Visibility = isVisible;
                E_Button.Visibility = isVisible;
                F_Button.Visibility = isVisible;
                G_Button.Visibility = isVisible;
                H_Button.Visibility = isVisible;
                I_Button.Visibility = isVisible;
                J_Button.Visibility = isVisible;
                K_Button.Visibility = isVisible;
                L_Button.Visibility = isVisible;
                M_Button.Visibility = isVisible;
                N_Button.Visibility = isVisible;
                O_Button.Visibility = isVisible;
                P_Button.Visibility = isVisible;
                Q_Button.Visibility = isVisible;
                R_Button.Visibility = isVisible;
                S_Button.Visibility = isVisible;
                T_Button.Visibility = isVisible;
                U_Button.Visibility = isVisible;
                V_Button.Visibility = isVisible;
                W_Button.Visibility = isVisible;
                X_Button.Visibility = isVisible;
                Y_Button.Visibility = isVisible;
                Z_Button.Visibility = isVisible;
        }
		private string NewWord()
        {
            int choice;
            string lastWord = answer;
            string chosenWord = answer;

            while (lastWord == chosenWord)
            {
                choice = rand.Next(0, wordlist.Count() - 1);
                chosenWord = wordlist[choice];
            }

            return chosenWord;
		}
		
		private bool LetterPosition(char guess)
        {
            string guessString = guess.ToString();
            bool letterFound = false;

            for (int i = 0; i < answerLength; i++)
            {
                switch (i)
                {
                    case 0:
                        {
                            if (answer[i] == guess)
                            {
                                this.AnswerChar1.Text = guess.ToString();
                                this.AnswerChar1.Foreground = new SolidColorBrush(Colors.Cyan);
                                letterFound = true;
                                charsGuessed++;
                            }

                            break;
                        }

                    case 1:
                        {
                            if (answer[i] == guess)
                            {
                                this.AnswerChar2.Text = guess.ToString();
                                this.AnswerChar2.Foreground = new SolidColorBrush(Colors.Cyan);
                                letterFound = true;
                                charsGuessed++;
                            }

                            break;
                        }

                    case 2:
                        {
                            if (answer[i] == guess)
                            {
                                this.AnswerChar3.Text = guess.ToString();
                                this.AnswerChar3.Foreground = new SolidColorBrush(Colors.Cyan);
                                letterFound = true;
                                charsGuessed++;
                            }

                            break;
                        }

                    case 3:
                        {
                            if (answer[i] == guess)
                            {
                                this.AnswerChar4.Text = guess.ToString();
                                this.AnswerChar4.Foreground = new SolidColorBrush(Colors.Cyan);
                                letterFound = true;
                                charsGuessed++;
                            }

                            break;
                        }

                    case 4:
                        {
                            if (answer[i] == guess)
                            {
                                this.AnswerChar5.Text = guess.ToString();
                                this.AnswerChar5.Foreground = new SolidColorBrush(Colors.Cyan);
                                letterFound = true;
                                charsGuessed++;
                            }

                            break;
                        }

                    case 5:
                        {
                            if (answer[i] == guess)
                            {
                                this.AnswerChar6.Text = guess.ToString();
                                this.AnswerChar6.Foreground = new SolidColorBrush(Colors.Cyan);
                                letterFound = true;
                                charsGuessed++;
                            }

                            break;
                        }

                    case 6:
                        {
                            if (answer[i] == guess)
                            {
                                this.AnswerChar7.Text = guess.ToString();
                                this.AnswerChar7.Foreground = new SolidColorBrush(Colors.Cyan);
                                letterFound = true;
                                charsGuessed++;
                            }

                            break;
                        }

                    case 7:
                        {
                            if (answer[i] == guess)
                            {
                                this.AnswerChar8.Text = guess.ToString();
                                this.AnswerChar8.Foreground = new SolidColorBrush(Colors.Cyan);
                                letterFound = true;
                                charsGuessed++;
                            }

                            break;
                        }

                    case 8:
                        {
                            if (answer[i] == guess)
                            {
                                this.AnswerChar9.Text = guess.ToString();
                                this.AnswerChar9.Foreground = new SolidColorBrush(Colors.Cyan);
                                letterFound = true;
                                charsGuessed++;
                            }

                            break;
                        }

                    case 9:
                        {
                            if (answer[i] == guess)
                            {
                                this.AnswerChar10.Text = guess.ToString();
                                this.AnswerChar10.Foreground = new SolidColorBrush(Colors.Cyan);
                                letterFound = true;
                                charsGuessed++;
                            }

                            break;
                        }


                }
            }

            //next stage of hangman if letter not found
            if (letterFound)
            {
            }
            else
            {
                incorrectCount++;
                ChangeHangmanPicture(incorrectCount);
            }

            if (incorrectCount > 10)
            {
                ChangeWinPicture(2);
                LetterVisible(false);
                Win_Picture.Visibility = System.Windows.Visibility.Visible;
                ShowAnswer();
                EndGame.Text = "You've lost, try again?";
                EndGame.Visibility = System.Windows.Visibility.Visible;
                SetGameCount(false, answerLength, incorrectCount);
            }

            if (charsGuessed == answerLength)
            {
                ChangeWinPicture(1);
                LetterVisible(false);
                Win_Picture.Visibility = System.Windows.Visibility.Visible;
                EndGame.Text = "Congratulations, You Won!";
                EndGame.Visibility = System.Windows.Visibility.Visible;
                SetGameCount(true, answerLength, incorrectCount);
            }

            if ((charsGuessed == answerLength) & (incorrectCount == 0))
            {
                ChangeWinPicture(0);
                EndGame.Text = "Wow! Are You Cheating???";
                EndGame.Visibility = System.Windows.Visibility.Visible;
            }

            return letterFound;
		}


        private void ChangeHangmanPicture(int hangmanStage)
        {

            switch (hangmanStage)
            {
                case 0:
                    {
                        Hangman_Picture.Source = new BitmapImage(new Uri(@"Assets/hangmanstart.png", UriKind.RelativeOrAbsolute));

                        break;
                    }

                case 1:
                    {
                        Hangman_Picture.Source = new BitmapImage(new Uri(@"Assets/hangman1.png", UriKind.RelativeOrAbsolute));
                        break;
                    }

                case 2:
                    {
                        Hangman_Picture.Source = new BitmapImage(new Uri(@"Assets/hangman2.png", UriKind.RelativeOrAbsolute));
                        break;
                    }

                case 3:
                    {
                        Hangman_Picture.Source = new BitmapImage(new Uri(@"Assets/hangman3.png", UriKind.RelativeOrAbsolute));
                        break;
                    }

                case 4:
                    {
                        Hangman_Picture.Source = new BitmapImage(new Uri(@"Assets/hangman4.png", UriKind.RelativeOrAbsolute));
                        break;
                    }

                case 5:
                    {
                        Hangman_Picture.Source = new BitmapImage(new Uri(@"Assets/hangman5.png", UriKind.RelativeOrAbsolute));
                        break;
                    }

                case 6:
                    {
                        Hangman_Picture.Source = new BitmapImage(new Uri(@"Assets/hangman6.png", UriKind.RelativeOrAbsolute));
                        break;
                    }

                case 7:
                    {
                        Hangman_Picture.Source = new BitmapImage(new Uri(@"Assets/hangman7.png", UriKind.RelativeOrAbsolute));
                        break;
                    }

                case 8:
                    {
                        Hangman_Picture.Source = new BitmapImage(new Uri(@"Assets/hangman8.png", UriKind.RelativeOrAbsolute));
                        break;
                    }

                case 9:
                    {
                        Hangman_Picture.Source = new BitmapImage(new Uri(@"Assets/hangman9.png", UriKind.RelativeOrAbsolute));
                        break;
                    }

                case 10:
                    {
                        Hangman_Picture.Source = new BitmapImage(new Uri(@"Assets/hangman10.png", UriKind.RelativeOrAbsolute));
                        break;
                    }

                case 11:
                    {
                        Hangman_Picture.Source = new BitmapImage(new Uri(@"Assets/hangmanend.png", UriKind.RelativeOrAbsolute));
                        break;
                    }

            }

        }

        private void ChangeWinPicture(int winStage)
        {

            switch (winStage)
            {
                case 0:
                    {
                        Win_Picture.Source = new BitmapImage(new Uri(@"Assets/win1.png", UriKind.RelativeOrAbsolute));

                        break;
                    }

                case 1:
                    {
                        Win_Picture.Source = new BitmapImage(new Uri(@"Assets/win2.png", UriKind.RelativeOrAbsolute));
                        break;
                    }

                case 2:
                    {
                        Win_Picture.Source = new BitmapImage(new Uri(@"Assets/win3.png", UriKind.RelativeOrAbsolute));
                        break;
                    }
            }

        }

        private void badgeChange(string badgeName, int badgeStage)
        {
            //NewKid Badge
            if (badgeName == "NewKid")
            {
                if (badgeStage == 0)
                {
                    NewKid_Picture.Source = new BitmapImage(new Uri(@"Assets/Achievements/NewKid.png", UriKind.RelativeOrAbsolute));
                }

                if (badgeStage == 1)
                {
                    NewKid_Picture.Source = new BitmapImage(new Uri(@"Assets/Achievements/NewKidEarned.png", UriKind.RelativeOrAbsolute));
                }
            }

            //AppChallenge Badge
            if (badgeName == "AppChallenge")
            {
                if (badgeStage == 0)
                {
                    AppChallenge_Picture.Source = new BitmapImage(new Uri(@"Assets/Achievements/AppChallenge.png", UriKind.RelativeOrAbsolute));
                }

                if (badgeStage == 1)
                {
                    AppChallenge_Picture.Source = new BitmapImage(new Uri(@"Assets/Achievements/AppChallengeEarned.png", UriKind.RelativeOrAbsolute));
                }
            }

            //AppChallenge Badge
            if (badgeName == "RowWin")
            {
                if ((badgeStage < 10)  && (maxWinsRow < 10))
                {
                    RowWin_Picture.Source = new BitmapImage(new Uri(@"Assets/Achievements/TenTimes.png", UriKind.RelativeOrAbsolute));
                }

                if ((badgeStage > 9) || (maxWinsRow > 9))
                {
                    RowWin_Picture.Source = new BitmapImage(new Uri(@"Assets/Achievements/TenTimesEarned.png", UriKind.RelativeOrAbsolute));

                    if (badgeStage > maxWinsRow)
                    {
                        maxWinsRow = badgeStage;
                    }
                }
            }

            // Count Badge
            if (badgeName == "Count")
            {
                if (badgeStage == 0)
                {
                    Count_Picture.Source = new BitmapImage(new Uri(@"Assets/Achievements/Centurion.png", UriKind.RelativeOrAbsolute));
                }

                if ((badgeStage > 99) && (badgeStage < 1000))
                {
                    Count_Picture.Source = new BitmapImage(new Uri(@"Assets/Achievements/CenturionEarned.png", UriKind.RelativeOrAbsolute));
                }

                if ((badgeStage > 999) && (badgeStage < 1000000))
                {
                    Count_Picture.Source = new BitmapImage(new Uri(@"Assets/Achievements/ThousandEarned.png", UriKind.RelativeOrAbsolute));
                }

                if (badgeStage > 999999)
                {
                    Count_Picture.Source = new BitmapImage(new Uri(@"Assets/Achievements/MillionareEarned.png", UriKind.RelativeOrAbsolute));
                }
            }

            // Loser Badge
            if (badgeName == "Loser")
            {
                if (badgeStage == 0)
                {
                    Loser_Picture.Source = new BitmapImage(new Uri(@"Assets/Achievements/OhDear.png", UriKind.RelativeOrAbsolute));
                }

                if (badgeStage > 49)
                {
                    Loser_Picture.Source = new BitmapImage(new Uri(@"Assets/Achievements/OhDearEarned.png", UriKind.RelativeOrAbsolute));
                }

            }


            // Pro Badge
            if (badgeName == "Pro")
            {
                if (badgeStage == 0)
                {
                    Pro_Picture.Source = new BitmapImage(new Uri(@"Assets/Achievements/Pro.png", UriKind.RelativeOrAbsolute));
                }

                if (badgeStage == 1)
                {
                    Pro_Picture.Source = new BitmapImage(new Uri(@"Assets/Achievements/ProEarned.png", UriKind.RelativeOrAbsolute));
                }

            }


        }

        private void NewGame_Click(object sender, RoutedEventArgs e)
        {
            resetApp();
        }

        private void ShowAnswer()
        {

            for (int i = 0; i < answerLength; i++)
            {
                switch (i)
                {
                    case 0:
                        {

                            if (AnswerChar1.Text == "?")
                            {
                                AnswerChar1.Text = answer[i].ToString();
                                AnswerChar1.Foreground = new SolidColorBrush(Colors.Red);
                            }
                            break;
                        }

                    case 1:
                        {

                            if (AnswerChar2.Text == "?")
                            {
                                AnswerChar2.Text = answer[i].ToString();
                                AnswerChar2.Foreground = new SolidColorBrush(Colors.Red);
                            }
                            break;
                        }

                    case 2:
                        {

                            if (AnswerChar3.Text == "?")
                            {
                                AnswerChar3.Text = answer[i].ToString();
                                AnswerChar3.Foreground = new SolidColorBrush(Colors.Red);
                            }
                            break;
                        }

                    case 3:
                        {

                            if (AnswerChar4.Text == "?")
                            {
                                AnswerChar4.Text = answer[i].ToString();
                                AnswerChar4.Foreground = new SolidColorBrush(Colors.Red);
                            }
                            break;
                        }


                    case 4:
                        {

                            if (AnswerChar5.Text == "?")
                            {
                                AnswerChar5.Text = answer[i].ToString();
                                AnswerChar5.Foreground = new SolidColorBrush(Colors.Red);
                            }
                            break;
                        }


                    case 5:
                        {

                            if (AnswerChar6.Text == "?")
                            {
                                AnswerChar6.Text = answer[i].ToString();
                                AnswerChar6.Foreground = new SolidColorBrush(Colors.Red);
                            }
                            break;
                        }


                    case 6:
                        {

                            if (AnswerChar7.Text == "?")
                            {
                                AnswerChar7.Text = answer[i].ToString();
                                AnswerChar7.Foreground = new SolidColorBrush(Colors.Red);
                            }
                            break;
                        }


                    case 7:
                        {

                            if (AnswerChar8.Text == "?")
                            {
                                AnswerChar8.Text = answer[i].ToString();
                                AnswerChar8.Foreground = new SolidColorBrush(Colors.Red);
                            }
                            break;
                        }


                    case 8:
                        {

                            if (AnswerChar9.Text == "?")
                            {
                                AnswerChar9.Text = answer[i].ToString();
                                AnswerChar9.Foreground = new SolidColorBrush(Colors.Red);
                            }
                            break;
                        }


                    case 9:
                        {

                            if (AnswerChar10.Text == "?")
                            {
                                AnswerChar10.Text = answer[i].ToString();
                                AnswerChar10.Foreground = new SolidColorBrush(Colors.Red);
                            }
                            break;
                        }
                }
            }

        }

        private void SetGameCount(bool wonGame, int wordLength, int incorrect)
        {
            gamesPlayed++;

            GamesPlayed.Text = gamesPlayed.ToString();

            if (gamesPlayed == 1)
            {
                newkidStage = 1;
                badgeChange("NewKid", newkidStage);
            }

            if ((incorrect == 0) && (proStage == 0))
            {
                proStage = 1;
                badgeChange("Pro", proStage);
            }

              
            if (wonGame)
            {
                gamesWon++;
                GamesWon.Text = gamesWon.ToString();
                badgeChange("Count", gamesWon);

                winsInRow++;
                badgeChange("RowWin", winsInRow);
            }
            else
            {
                gamesLost++;
                GamesLost.Text = gamesLost.ToString();
                badgeChange("Loser", gamesLost);

                winsInRow = 0;
            }

            if (wordLength > gamesWordLength)
            {
                gamesWordLength = wordLength;
                LongestWord.Text = gamesWordLength.ToString();

            }


            //if ((stage < gamesEarliestStage) || (gamesEarliestStage == 0))
            //{
            //    gamesEarliestStage = stage;
            //    LowestHangmanStage.Text = gamesEarliestStage.ToString();
            //}

            //Write out XML File

            var xml = @"<?xml version=""1.0"" encoding=""utf-8"" ?>
            <hangman>
            <file>Hangman Scores</file>
            <item>
            <title>Games Played</title>
            <score>" + GamesPlayed.Text + @"</score>
            </item>
            <item>
            <title>Games Won</title>
            <score>" + GamesWon.Text + @"</score>
            </item>
            <item>
            <title>Wins In Row</title>
            <score>" + winsInRow + @"</score>
            </item>
            <item>
            <title>Games Lost</title>
            <score>" + GamesLost.Text + @"</score>
            </item>
            <item>
            <title>Longest Word</title>
            <score>" + LongestWord.Text + @"</score>
            </item>
            <item>
            <title>NewKid Badge</title>
            <score>" + newkidStage + @"</score>
            </item>
            <item>
            <title>AppChallenge Badge</title>
            <score>" + appChallengeStage + @"</score>
            </item>
            <item>
            <title>Pro Badge</title>
            <score>" + proStage + @"</score>
            </item>
            <item>
            <title>Row Badge</title>
            <score>" + maxWinsRow + @"</score>
            </item>
            </hangman>";

            XDocument myXmlDocument = XDocument.Parse(xml);

            // Get the local folder.
            System.IO.IsolatedStorage.IsolatedStorageFile local =
                System.IO.IsolatedStorage.IsolatedStorageFile.GetUserStoreForApplication();

            // Create a new folder named DataFolder.
            if (!local.DirectoryExists("DataFolder"))
                local.CreateDirectory("DataFolder");

            // Create a new file named DataFile.txt.
            using (var isoFileStream =
                    new System.IO.IsolatedStorage.IsolatedStorageFileStream(
                        "DataFolder\\Scores.xml",
                        System.IO.FileMode.OpenOrCreate,
                            local))
            {
                // Write out the XML File.
                    myXmlDocument.Save(isoFileStream);
                    isoFileStream.Close();

            }

        }
            

        private void TextBlock_Tap(object sender, GestureEventArgs e)
        {
            //work out text
            var control = (sender as TextBlock);

            MessageBox.Show(control.Text + ", was tapped!", "Happyness", MessageBoxButton.OK);

            if (control.Text == "Animals")
            {
                wordlist = ANIMALS;
            }


            if (control.Text == "Fruit & Vegetables")
            {
                wordlist = VEGETABLES;
            }

            if (control.Text == "Minerals")
            {
                wordlist = MINERALS;
            }

            if (control.Text == "Citiess")
            {
                wordlist = CITIES;
            }

            if (control.Text == "Numbers")
            {
                wordlist = NUMBERS;
            }

            if (control.Text == "Trees")
            {
                wordlist = TREES;
            }

            if (control.Text == "KS1 Words")
            {
                wordlist = KS1;
            }

            if ((charsGuessed > 0) && (EndGame.Text == ""))
            {
                MessageBox.Show("Category will be changed after the current game", "Category Change", MessageBoxButton.OK);
            }
            else
            {
                resetApp();
            }

            control.Foreground = new SolidColorBrush(Colors.Cyan);
            PivotControl.SelectedIndex = 1;

            //check if game in progress

            //set category & prepare for new game


        }

        private void LongListSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Get item of LongListSelector. 
            List<UserControl> listItems = new List<UserControl>();
            GetItemsRecursive<UserControl>(MyLongListSelector, ref listItems);


            // Selected. 
            if (e.AddedItems.Count > 0 && e.AddedItems[0] != null)
            {
                foreach (UserControl userControl in listItems)
                {
                    if (e.AddedItems[0].Equals(userControl.DataContext))
                    {
                        VisualStateManager.GoToState(userControl, "Selected", true);

                        if (e.AddedItems[0].ToString() == "Animals")
                        {
                            wordlist = ANIMALS;
                        }


                        if (e.AddedItems[0].ToString() == "Fruit & Vegetables")
                        {
                            wordlist = VEGETABLES;
                        }

                        if (e.AddedItems[0].ToString() == "Minerals")
                        {
                            wordlist = MINERALS;
                        }

                        if (e.AddedItems[0].ToString() == "Cities")
                        {
                            wordlist = CITIES;
                        }

                        if (e.AddedItems[0].ToString() == "Numbers")
                        {
                            wordlist = NUMBERS;
                        }

                        if (e.AddedItems[0].ToString() == "Trees")
                        {
                            wordlist = TREES;
                        }

                        if (e.AddedItems[0].ToString() == "KS1 Words")
                        {
                            wordlist = KS1;
                        }

                        if ((charsGuessed > 0) && (EndGame.Text == ""))
                        {
                            MessageBox.Show("Category will be changed after the current game", "Category Change", MessageBoxButton.OK);
                        }
                        else
                        {
                            resetApp();
                        }

                        PivotControl.SelectedIndex = 1;

                    }
                }
            }
            // Unselected. 
            if (e.RemovedItems.Count > 0 && e.RemovedItems[0] != null)
            {
                foreach (UserControl userControl in listItems)
                {
                    if (e.RemovedItems[0].Equals(userControl.DataContext))
                    {
                        VisualStateManager.GoToState(userControl, "Normal", true);
                    }
                }
            }
        }


        /// <summary> 
        /// Recursive get the item. 
        /// </summary> 
        /// <typeparam name="T">The item to get.</typeparam> 
        /// <param name="parents">Parent container.</param> 
        /// <param name="objectList">Item list</param> 
        public static void GetItemsRecursive<T>(DependencyObject parents, ref List<T> objectList) where T : DependencyObject
        {
            var childrenCount = VisualTreeHelper.GetChildrenCount(parents);


            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parents, i);


                if (child is T)
                {
                    objectList.Add(child as T);
                }


                GetItemsRecursive<T>(child, ref objectList);
            }


            return;
        }
        private void randomCategory()
        {
            // Need to add randomiser logic
            int choice = rand.Next(1, 7);

            switch (choice)
            {
                case 1:
                    {
                        wordlist = ANIMALS;
                        break;
                    }

                case 2:
                    {
                        wordlist = VEGETABLES;
                        break;
                    }

                case 3:
                    {
                        wordlist = MINERALS;
                        break;
                    }

                case 4:
                    {
                        wordlist = CITIES;
                        break;
                    }

                case 5:
                    {
                        wordlist = NUMBERS;
                        break;
                    }

                case 6:
                    {
                        wordlist = KS1;
                        break;
                    }

                case 7:
                    {
                        wordlist = TREES;
                        break;
                    }

            }

 
            //MyLongListSelector.SelectedItem = choice - 1;


        }

        async void linktoAppChallenge(object sender, RoutedEventArgs e)
        {

            //Update XML scores file with indicator for Appchallenge badge

            writeAppChallenge();

            ////throw new NotSupportedException("Failed to access badge file.");

            
            // Launch URI.
            await Windows.System.Launcher.LaunchUriAsync(new Uri("appchallengeuk:appGuid=64453f0b-5365-46b0-a0a0-033abd3ef8fe", UriKind.Absolute)); //(without the brackets)
        }

        private void supportEmail_OnClick(object sender, RoutedEventArgs e)
        {
            EmailComposeTask emailComposeTask = new EmailComposeTask();
            emailComposeTask.Subject = "Hangman Neon Phone App Support";
            emailComposeTask.Body = "(please give some details about the support or feature you would like for this App)";
            emailComposeTask.To = "Support@Horus-Software.co.uk";
            emailComposeTask.Show();
        }

        private void PivotControl_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void writeAppChallenge()
        {
            //Set AppChallenge flag
            appChallengeStage = 1;
            badgeChange("AppChallenge", appChallengeStage);

            //Construct XML File

            var xml = @"<?xml version=""1.0"" encoding=""utf-8"" ?>
            <hangman>
            <file>Hangman Scores</file>
            <item>
            <title>Games Played</title>
            <score>" + GamesPlayed.Text + @"</score>
            </item>
            <item>
            <title>Games Won</title>
            <score>" + GamesWon.Text + @"</score>
            </item>
            <item>
            <title>Wins In Row</title>
            <score>" + winsInRow + @"</score>
            </item>
            <item>
            <title>Games Lost</title>
            <score>" + GamesLost.Text + @"</score>
            </item>
            <item>
            <title>Longest Word</title>
            <score>" + LongestWord.Text + @"</score>
            </item>
            <item>
            <title>NewKid Badge</title>
            <score>" + newkidStage + @"</score>
            </item>
            <item>
            <title>AppChallenge Badge</title>
            <score>" + appChallengeStage + @"</score>
            </item>
            <item>
            <title>Pro Badge</title>
            <score>" + proStage + @"</score>
            </item>
            <item>
            <title>Row Badge</title>
            <score>" + maxWinsRow + @"</score>
            </item>
            </hangman>";

            XDocument myXmlDocument = XDocument.Parse(xml);

            // Get the local folder.
            System.IO.IsolatedStorage.IsolatedStorageFile local =
                System.IO.IsolatedStorage.IsolatedStorageFile.GetUserStoreForApplication();

            // Create a new folder named DataFolder.
            if (!local.DirectoryExists("DataFolder"))
                local.CreateDirectory("DataFolder");

            // Create a new file named DataFile.txt.
            using (var isoFileStream =
                    new System.IO.IsolatedStorage.IsolatedStorageFileStream(
                        "DataFolder\\Scores.xml",
                        System.IO.FileMode.OpenOrCreate,
                            local))
            {
                // Write out the XML File.
                myXmlDocument.Save(isoFileStream);
                isoFileStream.Close();

            }

        }

        void AdUnit_ErrorOccurred(object sender, Microsoft.Advertising.AdErrorEventArgs e)
        {

            //Consider replacing microsoft Ad if not available.
            if (e.Error.Message == "No ad available.")
            {
            }
// make sure app doesn't crash JUST because no internet connectivity
            else if ((e.Error.Message != "") && (e.Error.Message != "HTTP error status code: NotFound (404)"))
            {
                throw new NotSupportedException("Error with Microsoft Ad on Category Pivot: " + e.Error.Message);
            }
        }

        void AdUnit2_ErrorOccurred(object sender, Microsoft.Advertising.AdErrorEventArgs e)
        {

            //Consider replacing microsoft Ad if not available.
            if (e.Error.Message == "No ad available.")
            {
            }
            // make sure app doesn't crash JUST because no internet connectivity
            else if ((e.Error.Message != "") && (e.Error.Message != "HTTP error status code: NotFound (404)"))
            {
                throw new NotSupportedException("Error with Microsoft Ad on Play Pivot: " + e.Error.Message);
            }
        }

        void AdUnit3_ErrorOccurred(object sender, Microsoft.Advertising.AdErrorEventArgs e)
        {

            //Consider replacing microsoft Ad if not available.
            if (e.Error.Message == "No ad available.")
            {
            }
            // make sure app doesn't crash JUST because no internet connectivity
            else if ((e.Error.Message != "") && (e.Error.Message != "HTTP error status code: NotFound (404)"))
            {
                throw new NotSupportedException("Error with Microsoft Ad on Achievements Pivot: " + e.Error.Message);
            }
        }

    }



}