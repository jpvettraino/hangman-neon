﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.ComponentModel;
using HangmanNeon.Resources;

namespace HangmanNeon.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public MainViewModel()
        {
            this.Items = new ObservableCollection<ItemViewModel>();
        }

        /// <summary>
        /// A collection for ItemViewModel objects.
        /// </summary>
        public ObservableCollection<ItemViewModel> Items { get; private set; }

        private string _sampleProperty = "Sample Runtime Property Value";
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding
        /// </summary>
        /// <returns></returns>
        public string SampleProperty
        {
            get
            {
                return _sampleProperty;
            }
            set
            {
                if (value != _sampleProperty)
                {
                    _sampleProperty = value;
                    NotifyPropertyChanged("SampleProperty");
                }
            }
        }

        /// <summary>
        /// Sample property that returns a localized string
        /// </summary>
        public string LocalizedSampleProperty
        {
            get
            {
                return AppResources.SampleProperty;
            }
        }

        public bool IsDataLoaded
        {
            get;
            private set;
        }

        /// <summary>
        /// Creates and adds a few ItemViewModel objects into the Items collection.
        /// </summary>
        public void LoadData()
        {
            // Sample data; replace with real data
            //this.Items.Add(new ItemViewModel() { LineOne = "Animals"});
            //this.Items.Add(new ItemViewModel() { LineOne = "Vegetables"});
            //this.Items.Add(new ItemViewModel() { LineOne = "Minerals" });
            //this.Items.Add(new ItemViewModel() { LineOne = "Cities" });
            //this.Items.Add(new ItemViewModel() { LineOne = "Numbers" });
            //this.Items.Add(new ItemViewModel() { LineOne = "KS1 Words" });

            //// Test data.
            //List<string> listData = new List<string>();
            //listData.Add("ASPNET");
            //listData.Add("WCF");
            //listData.Add("WPF");
            //listData.Add("Windows Store");
            //listData.Add("Windows Phone");

            //// Bind to control.
            //MyLongListSelector.ItemsSource = listData;
            this.IsDataLoaded = true;

        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}