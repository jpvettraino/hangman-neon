This classic Hangman game offers a range of categories and over 2000 words to keep you guessing.

The categroies include the words used for Key Stage 1 (KS1) and Key Stage 2 (KS2); providing a fun way for children to learn words they are regularly tested on.